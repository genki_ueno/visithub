FactoryBot.define do
  factory :card do
    title { "MyString" }
    image { "MyString" }
    genre_id { 1 }
    url { "MyString" }
  end
end
