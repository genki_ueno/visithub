class CreateCards < ActiveRecord::Migration[5.2]
  def change
    create_table :cards do |t|
      t.string :title
      t.text :content
      t.integer :genre_id
      t.string :url
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :cards, [:genre_id]
  end
end
