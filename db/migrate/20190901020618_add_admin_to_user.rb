class AddAdminToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :name, :string, default: 'No Name'
    add_column :users, :admin, :boolean, default: false
  end
end
