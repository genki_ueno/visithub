Rails.application.routes.draw do
  get 'users/show'
  devise_for :users
  root 'home#index'

  resources :home, only: [:index, :show] do
    get :likes, on: :collection
    get :close, on: :collection
    get :open,  on: :collection
  end

  resources :users, onlu: :show do
    get :posts,  on: :member
    get :likes,  on: :member
    get :system, on: :member
  end

  resources :cards do
    get :open_modal,         on: :member
    get :close_modal,        on: :member
    get :create_like,        on: :member
    get :delete_like,        on: :member
    get :detail_create_like, on: :member
    get :detail_delete_like, on: :member
  end

  resources :likes
end
