module ApplicationHelper
  def card_genres
    [['Eat & Drink', 0], ['See & Do', 1], ['Events', 2], ['Other', 3]]
  end

  def modal_line_num(index)
    if    index <= 2
      '1'
    elsif index <= 5
      '2'
    elsif index <= 8
      '3'
    elsif index <= 11
      '4'
    elsif index <= 14
      '5'
    end
  end
end
