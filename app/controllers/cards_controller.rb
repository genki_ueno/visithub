class CardsController < ApplicationController
  before_action :authenticate_user!, except: [:open_modal, :close_modal]
  before_action :set_card, only: [:show, :edit, :open_modal, :close_modal, :create_like, :delete_like, :detail_create_like, :detail_delete_like]
  def new
    @card = Card.new
  end
  def create
    @card = current_user.cards.build(card_params)
    if @card.save
      redirect_to root_path
    else
      render 'new'
    end
  end
  def show
  end
  def edit
    @card = Card.find(params[:id])
  end
  def update
    @card = Card.find(params[:id])
    @card.images.first.destroy if @card.images.first
    @card.update(card_params)
    redirect_to root_path
  end
  def destroy
    Card.find(params[:id]).destroy
    redirect_to posts_user_path(current_user)
  end
  def open_modal
  end
  def close_modal
  end
  def create_like
    current_user.likes.find_or_create_by(card_id: @card.id)
  end
  def delete_like
    Like.find_by(card_id: @card.id, user_id: current_user).destroy
  end
  def detail_create_like
    current_user.likes.find_or_create_by(card_id: @card.id)
  end
  def detail_delete_like
    Like.find_by(card_id: @card.id, user_id: current_user).destroy
  end

  private
    def card_params
      params.require(:card).permit(:title, :content, :genre_id, :user_id, :url, images: [])
    end
    def set_card
      @card = Card.find(params[:id])
    end
end
