class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :admin_check, only: :system
  def posts
    @post_cards = Card.where(user_id: current_user.id).page(params[:page])
  end

  def likes
    @like_cards = current_user.like_cards.page(params[:page])
  end

  def system
    @users = User.all
  end

  private

  def admin_check
    user = User.find(current_user.id)
    redirect_to root_path unless user.admin
  end
end
