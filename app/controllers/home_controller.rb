class HomeController < ApplicationController
  def index
    @card  = Card.new
    @q     = Card.all.ransack(params[:q])
    @cards = @q.result(distinct: true).page(params[:page])
  end

  def show
    @q     = Card.all.ransack(params[:q])
    @cards = Card.where(genre_id: params[:id]).page(params[:page])
    render 'index'
  end

  def likes
    @q     = Card.all.ransack(params[:q])
    @cards = @q.result(distinct: true).page(params[:page])
    render 'index'
  end

  def open
  end
  def close
  end
end
