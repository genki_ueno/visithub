class Card < ApplicationRecord
  belongs_to :user
  has_many :likes, dependent: :destroy
  has_many :liked_users, through: :likes, source: :user
  has_many_attached :images

  paginates_per 15

  CARD_GENRES = { 0 => 'Eat & Drink', 1 => 'See & Do', 2 => 'Events', 3 => 'Other' }

  def Card.genres(index)
    CARD_GENRES[index]
  end

  def Card.like_count(card_id)
    Card.find(card_id).likes.count
  end

  def Card.liked?(card_id, current_user)
    Like.find_by(card_id: card_id, user_id: current_user) ? true : false
  end
end
